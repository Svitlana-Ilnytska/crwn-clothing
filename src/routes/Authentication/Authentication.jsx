import SignUpForm from "../../components/SignUpForm/SignUpForm";
import SignInForm from "../../components/SignInForm/SignInForm";

import css from "./Authentication.module.scss";

const Authentication = () => {
  return (
    <div className={css.authenticationContainer}>
      <SignInForm />

      <SignUpForm />
    </div>
  );
};

export default Authentication;
