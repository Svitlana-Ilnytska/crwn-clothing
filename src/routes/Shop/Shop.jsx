import { useContext } from "react";

import { ProductsContext } from "../../contexts/ProductContext";
import ProductCard from '../../components/ProductCard/ProductCard'

import css from "./Shop.module.scss";

const Shop = () => {
  const { products } = useContext(ProductsContext);
  return (
    <div className={css.productsContainer}>
      {products.map((product) => (
       <ProductCard key={product.id} product={product}/>
      ))}
    </div>
  );
};

export default Shop;
