import { useContext } from "react";
import { Outlet, Link } from "react-router-dom";

import CartIcon from "../../components/CartIcon/CartIcon";
import CartDropdown from "../../components/CartDropdown/CartDropdown";

import { ReactComponent as CrwnLogo } from "../../assets/crown.svg";
import { UserContext } from "../../contexts/UserContext";
import { CartContext } from "../../contexts/CartContext";

import { signOutUser} from '../../utils/firebase/firebase'

import css from "./Navigation.module.scss";

const Navigation = () => {
  const {currentUser} = useContext(UserContext);
  const {isCartOpen} = useContext(CartContext);
  return (
    <>
      <div className={css.navigation}>
        <Link className={css.logoContainer} to="/">
          <CrwnLogo className={css.logo} />
        </Link>

        <div className={css.navLinksContainer}>
          <Link className={css.navLink} to="/shop">
            SHOP
          </Link>
          {
            currentUser ? (
              <span className={css.navLink} onClick={signOutUser}>SIGN OUT</span>
            ) : (
              <Link className={css.navLink} to="/auth">
            SIGN IN
          </Link>
            )
          }
          <CartIcon />
        </div>
        { isCartOpen && <CartDropdown />}
      </div>
      <Outlet />
    </>
  );
};

export default Navigation;
