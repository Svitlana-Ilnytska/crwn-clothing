import css from "./FormInput.module.scss";

const FormInput = ({ label, ...otherProps }) => {
  return (
    <div className={css.group}>
      {label && (
        <label
          className={`${
            otherProps.value.length ? css.shrink : ""
          } ${css.formInputLabel}`}
        >
          {label}
        </label>
      )}
      <input className={css.formInput} {...otherProps} />
    </div>
  );
};

export default FormInput;
