import css from "./CartItem.module.scss";

const CartItem = ({ cart }) => {
  const { name, imageUrl, price, quantity } = cart;
  return (
    <div className={css.cartItemContainer}>
      <img src={imageUrl} alt={`${name}`} />
      <div className={css.itemDetails}>
        <span className={css.name}>{name}</span>
        <span className={css.price}>
          {quantity} x ${price}
        </span>
      </div>
    </div>
  );
};

export default CartItem;
