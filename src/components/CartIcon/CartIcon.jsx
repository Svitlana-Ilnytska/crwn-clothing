import { useContext } from "react";

import { ReactComponent as ShoppingIcon } from "../../assets/shopping-bag.svg";
import { CartContext } from "../../contexts/CartContext";

import css from "./CartIcon.module.scss";

const CartIcon = () => {
    const {isCartOpen, setIsCartOpen, cartCount} = useContext(CartContext);
    const toogleIsCartOpen = () => setIsCartOpen(!isCartOpen);
  return (
    <div className={css.cartIconContainer} onClick={toogleIsCartOpen}>
      <ShoppingIcon className={css.ShoppingIcon} />
      <span className={css.itemCount}>{cartCount}</span>
    </div>
  );
};

export default CartIcon;
