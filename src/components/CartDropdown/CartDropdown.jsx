import { useContext } from "react";

import { CartContext } from "../../contexts/CartContext";
import Button from "../Button/Button";
import CartItem from "../CartItem/CartItem";

import css from "./CartDropdown.module.scss";

const CartDropdown = () => {
    const {cartItems} = useContext(CartContext);
  return (
    <div className={css.cartDropdownContainer}>
      <div className={css.cartItems}>
        {cartItems.map((item) => (
          <CartItem  key={item.id} cart={item} />
        ))}
      </div>
      <Button>GO TO CHECKOUT</Button>
    </div>
  );
};

export default CartDropdown;
