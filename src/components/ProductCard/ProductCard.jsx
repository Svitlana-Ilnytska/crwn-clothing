import { useContext } from "react";

import Button from "../Button/Button";
import { CartContext } from "../../contexts/CartContext";

import css from "./ProductCard.module.scss";

const ProductCard = ({ product }) => {
  const { name, price, imageUrl } = product;
  const { addItemToCart } = useContext(CartContext);

  const addProductToCart = () => addItemToCart(product);
  
  return (
    <div className={css.productCardContainer}>
      <img src={imageUrl} alt={`${name}`} />
      <div className={css.footer}>
        <span className={css.name}>{name}</span>
        <span className={css.price}>{price}</span>
      </div>
      <Button buttonType="inverted" onClick={addProductToCart}>
        Add to card
      </Button>
    </div>
  );
};

export default ProductCard;
