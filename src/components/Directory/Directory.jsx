import CategoryItem from "..//CategoryItem/CategoryItem";

import css from "./Directory.module.scss";

function Directory({ categories }) {
  return (
    <div className={css.directoryContainer}>
      {categories.map((category) => (
        <CategoryItem key={category.id} category={category} />
      ))}
    </div>
  );
}

export default Directory;
