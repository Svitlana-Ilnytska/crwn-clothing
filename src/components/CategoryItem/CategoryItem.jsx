import css from "./CategoryItem.module.scss";

function CategoryItem({ category }) {
  const { imageUrl, title } = category;
  return (
    <div className={css.categoryContainer}>
      <div
        className={css.backgroundImage}
        style={{ backgroundImage: `url(${imageUrl})` }}
      />
      <div className={css.categoryBodyContainer}>
        <h2>{title}</h2>
        <p>Shop now</p>
      </div>
    </div>
  );
}

export default CategoryItem;
